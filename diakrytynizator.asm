; --- Justyna Micota jm418427

; --- special mathematical constants
MODULO                  equ     0x10FF80
EXTRA_ARG               equ     0x80

; --- system defined values
STDIN                   equ     0
STDOUT                  equ     1
SYS_READ                equ     0
SYS_WRITE               equ     1

; --- values for checking presence of leading zeros:
; (maximum values that fit in x bytes of utf8 encoding)
LAST_ONEB               equ     0b1111111
LAST_TWOB               equ     0b11111111111
LAST_THREEB             equ     0b1111111111111111
LAST_FOURB              equ     0b100001111111111111111

; --- masks and values for byte processing:
FIVE_ONES               equ     248         ;11111000
FOUR_ONES               equ     240         ;11110000
THREE_ONES              equ     224         ;11100000
TWO_ONES                equ     192         ;11000000
LAST_SIX_BITS           equ     63          ;00111111
LAST_FIVE_BITS          equ     31          ;00011111
LAST_FOUR_BITS          equ     15          ;00001111
LAST_THREE_BITS         equ     7           ;00000111
FIRST_ONE               equ     128         ;10000000
SECOND_ONE              equ     64          ;01000000
THIRD_ONE               equ     32          ;00100000
FOURTH_ONE              equ     16          ;00010000

;R8, R9             - registers used most often for processing
;R10, R13           - extra registers for processing (often used to store values)
;R15                - register to perform mathematical calculations
;R12                - byte count during input / argument pointer before input
;R14                - integer value of a character


%macro doExit 1
	     mov    rax, 60
	     mov    rdi, %1             ;exit with exit code given in argument
	     syscall
%endmacro

; printing text from address given in 1st argument, 
; of length equal to number of bytes given in 2nd argument
%macro printText 2
	     mov    rax, SYS_WRITE
	     mov    rdi, STDOUT
         mov    rsi, %1             ;first argument points to what to print
         mov    rdx, %2             ;second argument indicates number of bytes to print
	     syscall
%endmacro

; getting one byte from input, it is stored at the 'inputByte' address
%macro getOneByte 0
         xor    rax, rax            ;rax = 0
         mov    [inputByte], rax    ;clear memory that holds input byte
         mov    rax, SYS_READ       
         mov    rdi, STDIN
         mov    rsi, inputByte      ;get one byte from input into address inputByte
         mov    rdx, 1      
         syscall
         test   rax, rax            ;look for the end of input
         jz     exit                ;exit with exit code 0
%endmacro

; converting binary value stored in six bits into 'following byte' of
; utf8 encoding - adds 10000000 to the 6bit value
; -- updates r14 (value of polynomial) - shifted 6 bits to the right
; -- updates r13 (adds final utf8 value of byte to this register)
; -- uses r8 and r9 as operating registers
%macro makeFollowingByteR13 0
         mov    r8, r14             ;get current value thats left of polynomial
         mov    r9, LAST_SIX_BITS   ;prepare mask for last six bits
         and    r9, r8              ;r9 = last six bits of r8
         add    r9, 128             ;converting r9 to utf8 following byte encoding
         shr    r8, 6               ;r8 = integer char value shifted 6 bits to the right
         mov    r14, r8             ;save value left to process
         add    r13, r9             ;add following byte to result
         shl    r13, 8              ;make space for next byte
%endmacro   

; checking if number of bytes that encode a character is equal to the given argument
; -- uses r9 as operating register
%macro checkNrOfBytes 1
         mov    r9, r10             ;get nr of bytes (stored in r10 in processLeadingByte)
         cmp    r9, %1              ;compare to argument 
         jne    error               ;should be exactly equal
         jmp    compute             ;if so, move to computing polynomial
%endmacro

; transforms one of the lit bits at the beginning of the leading byte in 
; utf8 encoding from 1 to 0
; the value to subtract to achieve this is given in argument
; -- uses r8 as operating register
; -- modifies value at the address 'inputByte'
%macro removeBitInTransformByte 1
         mov    r8b, [inputByte]    ;get the byte
         sub    r8b, %1             ;one of bits declaring number of bytes in utf8  
         mov    [inputByte], r8b    ;    encoding transformed from 1 to 0
%endmacro

; checks if a byte represents a digit
; -- uses rbx to extract value from address
; -- uses part of rcx for comparison
%macro checkIfDigit 0
         mov    rbx, r12            ;get the position in argument stack
         mov    cl, [rbx]           ;get one bit of the value on the position
         cmp    cl, 48              ;compare to '0'
         jb     error
         mov    cl, [rbx]           ;get one bit of the value on the position
         cmp    cl, 57              ;compare to '9'
         ja     error
%endmacro



section .bss
         inputByte      resb 8      ;for read/write and processing input/output byte
         sum            resb 8      ;used only during preprocessing (operating on args)
         argNum         resb 8      ;keeps number of arguments for polynomial

section .text
         global _start


_start:
         pop    r8                  ;get number of arguments from the stack
         pop    r15                 ;discard path
         dec    r8                  ;decrease nr of arguments (path was included)
         test   r8, r8
         jz     error               ;error if 0 arguments

         mov    [argNum], r8        ;save nr of arguments
         mov    r10, rsp            ;get pointer to arguments on stack

; argLoop: 
; -- R8 is a loop counter (initiated with nr of arguments)
; -- R10 points to positions in stack (pointers to text arguments)
; -- R12 points to bytes in text arguments
; -- R9, R15, RBX, RCX are short term use helper registers
argLoop:                            ; --- loop for processing arguments
         mov    r9, r8
         test   r9, r9              ;check if all arguments processed
         jz     inputLoop

         mov    r9, [r10]           ;take argument under current address on stack
         mov    r12, r9             ;remember position in the argument
         xor    rbx, rbx
         mov    [sum], rbx          ;sum = 0
         
convertArg:                         ; --- loop for processing one argument
         mov    rbx, r12            ;get pointer to next byte of arg
    
         xor    rcx, rcx
         mov    cl, [rbx]           ;get value under the pointer
         test   cl, cl              ;search for null pointer at the end of arg
         jz     finishedArg   

         checkIfDigit

         xor    rax, rax            ;clear rax
         mov    rax, [sum]          
         mov    r15, 10
         mul    r15                 ;multiply sum by 10

         sub    cl, 48              ;transform ascii character to digit
         add    rax, rcx            ;add it to sum

         xor    rdx, rdx            ;clear rdx for correct modulo
         mov    r15, MODULO       
         div    r15                 ;modulo the sum
         mov    [sum], rdx          ;remember new sum

         inc    r12                 ;increment the arg pointer (jump to next byte of arg)
         jmp    convertArg     

finishedArg:                        ; --- finalize processing of one argument
         mov    r15, [sum]
         mov    [r10], r15          ;save integer value of arg where pointer to text arg
                                    ;   was stored before
         add    r10, 8              ;move to next text argument pointer
         dec    r8                  ;decrease nr of arguments
         jmp argLoop

inputLoop:                          ; --- loop for reading next character
         getOneByte

         xor    rbx, rbx            ;clear rbx
         mov    rbx, [inputByte]   
         cmp    rbx, 128            ;check if its code starts with one
         je     error               ;1000 0000 is a wrong utf8 code for first byte
         jge    processLeadingByte  ;if greater, its a leading byte of longer utf8 code

         printText  inputByte, 1    ;else this character will not be changed
         jmp    inputLoop           ;get next character

; processLeadingByte:
; -- R12 counts number of bytes to read declared by first bits of leading bytes
; -- R10 stores number of bytes computed by R12 (for later use)
; -- R14 stores integer value of leading byte (converted from utf8 to binary,
;    shifted 6 * (R10 - 1) to the left)
; -- R8, R9 are short term use helper registers
; -- [inputByte] transformed while computing
processLeadingByte:                 ; --- determine how many bytes are in character 
                                    ; --- and store this value
         xor    rbx, rbx            ;clear rbx
         mov    rbx, [inputByte]    ;prepared inputByte to preprocess
         cmp    rbx, FIVE_ONES      ;check if 5 ones at the begginning (error if so)
         jae    error               

         xor    r12, r12            ;reset byte counter (in how many bytes char fits)

         removeBitInTransformByte FIRST_ONE
         inc    r12                 ;increase byte counter
         cmp    r8b, 64             ;look for next bit 1 in leading byte (2nd 1)
         jbe    error               ;10... (not allowed for leading byte)
         
         removeBitInTransformByte SECOND_ONE
         inc    r12                 ;increase byte counter
         cmp    r8b, 32             ;look for next bit 1 in leading byte (3rd 1)
         jb     beforeShift         ;exactly two bytes in utf8 of this character
         
         removeBitInTransformByte THIRD_ONE
         inc    r12                 ;increase byte counter
         cmp    r8b, 16             ;look for next bit 1 in leading byte (4th 1)
         jb     beforeShift         ;exactly three bytes in utf8 of this character
         
         removeBitInTransformByte FOURTH_ONE
         inc    r12                 ;increase byte counter
         cmp    r8b, 8              ;look for next bit 1 in leading byte (5th 1)
         jb     beforeShift         ;exactly four bytes in utf8 of this character

         jmp    error               ;more than four bytes in utf8 of this character

beforeShift:                        ; --- prepare values to shifting loop
         mov    r9, r12             ;get nr of bytes to r9
         dec    r9                  ;decrease byte counter (one byte already read)
         mov    r8, [inputByte]

shiftLeadingByte:                   ; --- shifting byte 6 bits to the left for 
                                    ; --- every following byte
         test   r9, r9              ;if no more bytes expected, finish
         jz     finishLeadingByte
         shl    r8, 6               ;make space for 6 bits from next byte
         dec    r9                  ;decrease nr of shifts to perform on this byte
         jmp    shiftLeadingByte

finishLeadingByte:                  ; --- initialize values for further processing
         mov    r14, r8             ;initialize r14 with computed value of leading byte
         mov    r10, r12            ;r10 stores number of all bytes of a character
         dec    r12                 ;r12 stores nr of bytes left to read
         
; getNextByte:
; -- R12 stores byte counter, decreased with every byte read
; -- R10 stores number of bytes representing this character declared by leading byte
; -- R14 stores increasing integer value of the whole character
; -- R8, R9 are short term use helper registers
; -- [inputByte] transformed while computing
getNextByte:
         mov    r9, r12             ;get nr of bytes
         test   r9, r9
         jz     checkLeadingZeros   ;if no more bytes to read, skip to next phase

         getOneByte                 ;getting next bytes of a character...

         mov    r8, [inputByte]     ;get the byte
         cmp    r8, 128             ;check if starts with 1
         jb     error               ;error if doesnt (wrong utf8 encoding)
         
         removeBitInTransformByte FIRST_ONE     ;remove part of utf8 special encoding
         
         mov    r8, [inputByte]
         cmp    r8, 64              ;check if second bit is 0
         jae     error              ;error if isnt (wrong utf8 encoding)
         
         mov    r9, r12             ;get nr of bytes left to get from user
         mov    r8, [inputByte]     ;get the current byte value

shiftFollowingByte:                 ; --- shifting byte 6 bits to the left for every 
                                    ; --- next byte to read
         dec    r9                  ;decrease nr of shifts to perform on this byte
         test   r9, r9              ;if no more bytes expected, finish
         jz     finishFollowingByte
         shl    r8, 6               ;make space for 6 bits from next byte
         jmp    shiftFollowingByte  
         
finishFollowingByte:                ; --- update values
         add    r14, r8             ;add value of this byte to value of a whole character
         dec    r12                 ;another byte read successfully
         jmp    getNextByte

; checkLeadingZeros:
; -- R14 stores computed integer value of a character
; -- R10 is used in checkNrOfBytes, stores nr of bytes that encode this character
; -- R8 is a helper short term use register
checkLeadingZeros:                  ; --- check if value encoded in a minimalistic way
         mov    r8, r14             ;get integer value of a character
         cmp    r8, LAST_ONEB       ;check if value should fit in 1 byte
         ja     checkForTwoBytes
         checkNrOfBytes 1

checkForTwoBytes:
         mov    r8, r14             ;get integer value of a character
         cmp    r8, LAST_TWOB       ;check if value should fit in 2 bytes
         ja     checkForThreeBytes
         checkNrOfBytes 2

checkForThreeBytes:
         mov    r8, r14             ;get integer value of a character
         cmp    r8, LAST_THREEB     ;check if value should fit in 3 bytes
         ja    checkForFourBytes
         checkNrOfBytes 3

checkForFourBytes:
         mov    r8, r14             ;get integer value of a character
         cmp    r8, LAST_FOURB      ;check if value fits in 4 bytes
         ja     error
         checkNrOfBytes 4

; compute:
; -- R14 stores polynomial argument during loop / computed polynomial at the end
; -- R13 is a loop counter (initiated with nr of arguments on stack)
; -- R10 stores address to arguments in stack
; -- R15, R8, R9 are short term use helper registers
compute:                            ; --- prepare to compute polynomial
         mov    r10, rsp            ;get address of first argument in stack      
         mov    r13, [argNum]       ;get arg number into loop counter (r13)

         xor    r9, r9
         mov    r9, 1               ;starting value of x^i (x^0)
         xor    r8, r8              ;stores result, equiv to r8 = 0
         
         mov    rax, r14
         sub    rax, EXTRA_ARG      ;compute (x - EXTRA_ARG) as argument for polynomial
         mov    r14, rax            ;save result

         xor    rdx, rdx            ;clear rdx for correct modulo
         mov    rax, r14            ;get integer value of a character
         mov    r15, MODULO       
         div    r15                 ;perform modulo on integer value of the character
         mov    r14, rdx            ;save result

computeLoop:                        ; --- the actual process of computing the polynomial
         test   r13, r13            ;out of arguments
         jz     finishComputing     ;w(x) computed

         mov    r15, [r10]          ;get argument
         mov    rax, r9             ;compute a_i * x^i
         mul    r15
         add    r8, rax             ;add the result to current sum

         xor    rdx, rdx            ;clear rdx for correct modulo
         mov    rax, r8             ;move current sum to rax register
         mov    r15, MODULO       
         div    r15                 ;modulo the (a_i * x^i)
         mov    r8, rdx             ;store the result
         
         mov    rax, r9 
         mul    r14                 ;compute x^i * x
         mov    r9, rax

         xor    rdx, rdx            ;clear rdx for correct modulo
         mov    rax, r9             ;move x^i * x to rax register
         mov    r15, MODULO
         div    r15                 ;modulo the (x^i * x)
         mov    r9, rdx             ;store the result

         add    r10, 8              ;move to next argument
         dec    r13                 ;decrease loop counter (nr of arguments left)
         jmp    computeLoop
        
finishComputing:
         mov    rax, r8
         add    rax, EXTRA_ARG      ;last phase of computing polynomial
         mov    r14, rax            ;save the computed integer value in r14

; convertToUTF8:
; -- R13 stores the result of encoding process with bytes in (!) reverse (!) order
; -- R12 counts bytes to print
; -- R14 stores value of polynomial (decreased during the process)
; -- makeFollowingByte modifies values of:
;    R13 (outcome byte added to this register), 
;    R14 (stores whats left of polynomial value), 
;    R8 (equal to R14, but processed shortly afterwards)
; -- R8, R9, R15 are short term use helper registers
convertToUTF8:                      ; --- converting to utf8 with reverse order of bytes
         mov    r12, 1              ;resetting R12 to count bytes to output
         
         mov    r13, r14            ;get value of polynomial
         cmp    r13, LAST_ONEB      ;compare to maximum 7 bit value
         jbe    printR13            ;dont convert if fits in 7 bits

         xor    r13, r13            ;clear r13
         inc    r12                 ;character will be at least 2 bytes long
         makeFollowingByteR13      

         cmp    r8, LAST_FIVE_BITS  ;compare to max value of 110 leading byte
         jbe    makeLeadingByte     ;character fits in 2 bytes
         inc    r12                 ;character will be at least 3 bytes long
         makeFollowingByteR13      

         cmp    r8, LAST_FOUR_BITS  ;compare to max value of 1110 leading byte
         jbe    makeLeadingByte     ;character fits in 3 bytes
         inc    r12                 ;character will be 4 bytes long
         makeFollowingByteR13      
         
         cmp    r8, LAST_THREE_BITS ;compare to max value of 11110 leading byte
         ja     error               ;character doesnt fit in 4 bytes

makeLeadingByte:                    ; --- turn whats left of polynomial into leading 
                                    ; --- byte in utf8 encoding
         mov    r8, r14             ;get whats left of polynomial
         mov    r15, r12            ;get the number of bytes
         cmp    r15, 2              ;check if encoding in two bytes
         je     addTwo
         mov    r15, r12            ;get the nr of bytes
         cmp    r15, 3              ;check if encoding in three bytes
         je     addThree
         mov    r15, r12            ;get the nr of bytes 
         cmp    r15, 4              ;check if encoding in four bytes
         je     addFour
         jmp    error               ;encoding in incorrect nr of bytes

addTwo:                             ; --- add 1s at the beginning of leading byte
         add    r8, TWO_ONES        ;add 11000000 to leading byte
         add    r13, r8             ;add leading byte to result
         jmp    printR13
addThree:
         add    r8, THREE_ONES      ;add 11100000 to leading byte
         add    r13, r8             ;add leading byte to result
         jmp    printR13
addFour: 
         add    r8, FOUR_ONES       ;add 11110000 to leading byte
         add    r13, r8             ;add leading byte to result

; printR13:
; -- R13 is final character in UTF8 in reversed order of bytes
; -- R12 is number of bytes to print
printR13: 
         mov    [inputByte], r13    ;put r13 in reverse order of bytes into inputByte
         printText  inputByte, r12  ;print the nr of bytes thats stored in r12
         jmp    inputLoop           ;get next character

exit:
         doExit   0
error:
         doExit   1